### EONET Weather map

![screenshot](/public/screenshot.png)

## Features in progress

* Sidebar to toggle different category pins

## Lessons learnt

* Inefficient way of rendering multiple types of icons. There should be a better way of handling differnet types of icons without having to generate separte files for each. But this would require importing the react-icons or svgs at some point.

## Unresolve issues

* Unable to resolve why version 3 of the API always have connection issues. Resort to using the deprecated version 2.1.

* unable to render correct coordinates for volcanoe data
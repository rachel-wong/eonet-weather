import React from 'react'
import Image from './spinner.gif'

const Loader = () => {
  return (
    <div className="loader">
      <img src={Image} alt="" />
      <h1>Fetching Data</h1>
    </div>
  )
}

export default Loader

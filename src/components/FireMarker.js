import React from 'react'
import { ImFire } from 'react-icons/im'

const FireMarker = ({ lat, lng, onClick }) => {
  return (
    <div className="location-marker fire" onClick={ onClick }>
      <ImFire className="location-icon"/>
    </div>
  )
}

export default FireMarker

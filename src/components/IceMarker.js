import React from 'react'
import { FaSnowflake } from 'react-icons/fa'

const IceMarker = ({ lat, lng, onClick }) => {
  return (
    <div className="location-marker ice" onClick={ onClick }>
      <FaSnowflake className="location-icon"/>
    </div>
  )
}

export default IceMarker

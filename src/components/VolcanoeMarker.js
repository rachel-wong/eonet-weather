import React from 'react'
import { WiVolcano } from 'react-icons/wi'

const VolcanoeMarker = ({ lat, lng, onClick }) => {
  return (
    <div className="location-marker volcanoe" onClick={ onClick }>
      <WiVolcano className="location-icon"/>
    </div>
  )
}

export default VolcanoeMarker

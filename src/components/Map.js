import React, { useState } from 'react'
import GoogleMapReact from 'google-map-react'
import FireMarker from './FireMarker'
import StormMarker from './StormMarker'
import LocationInfoBox from './LocationInfoBox'
import IceMarker from './IceMarker'
import VolcanoeMarker from './VolcanoeMarker'

const Map = ({ eventData, center, zoom }) => {
  const [locationInfo, setLocationInfo] = useState(null)

  const markers = eventData.map((ev, idx) => {
    switch (ev.categories[0].id) {
      case 8:
        return <FireMarker key={idx} lat={ev.geometries[0].coordinates[1]} lng={ev.geometries[0].coordinates[0]} onClick={() => setLocationInfo({ id: ev.id, title: ev.title })} />
      case 10:
        return <StormMarker key={idx} lat={ev.geometries[0].coordinates[1]} lng={ev.geometries[0].coordinates[0]} onClick={() => setLocationInfo({ id: ev.id, title: ev.title })} />
      case 15:
        return <IceMarker key={idx} lat={ev.geometries[0].coordinates[1]} lng={ev.geometries[0].coordinates[0]} onClick={() => setLocationInfo({ id: ev.id, title: ev.title })} />
      case 12:
        // return <VolcanoeMarker key={idx} lat={ev.geometries[0].coordinates[1]} lng={ev.geometries[0].coordinates[0]} onClick={() => setLocationInfo({ id: ev.id, title: ev.title })} />
      default:
        return null
    }
    // if (ev.categories[0].id === 8) {
    //   return <FireMarker key={idx} lat={ev.geometries[0].coordinates[1]} lng={ev.geometries[0].coordinates[0]} onClick={() => setLocationInfo({ id: ev.id, title: ev.title})}/>
    // }
    return null
  })
  return (
    <div className="map">
      <GoogleMapReact
        bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_MAP_API }}
        defaultCenter={center}
        defaultZoom={zoom}
      >
        { markers}
        {/* <LocationMarker lat={center.lat} lng={ center.lng}/> */}
      </GoogleMapReact>
      {locationInfo && <LocationInfoBox info={ locationInfo} />}
    </div>
  )
}
Map.defaultProps = {
  center: {
    lat: 42.3265,
    lng: -122.8756
  },
  zoom: 6
}
export default Map

import React from 'react'
import { IoIosThunderstorm } from 'react-icons/io'

const FireMarker = ({ lat, lng, onClick }) => {
  return (
    <div className="location-marker storm" onClick={ onClick }>
      <IoIosThunderstorm className="location-icon"/>
    </div>
  )
}

export default FireMarker

import './App.css';
import Map from './components/Map'
import { useState, useEffect } from 'react'
import axios from 'axios'
import Loader from './components/Loader'

function App() {
  const [eventData, setEventData] = useState([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const fetchEvents = async () => {
      setIsLoading(true)
      let { data } = await axios.get('https://eonet.sci.gsfc.nasa.gov/api/v2.1/events')
      console.log('data:', data.events)
      setEventData(data.events)
      setIsLoading(false)
    }
    fetchEvents()
  }, [])

  return (
    <div className="App">
      {!isLoading ? (<Map eventData={eventData} />)
        : (
          <Loader />
        )}
    </div>
  );
}

export default App;
